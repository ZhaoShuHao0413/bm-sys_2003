package com.qianfeng.web;

import com.alibaba.druid.pool.DruidPooledConnection;
import com.google.gson.Gson;
import com.qianfeng.entity.Bm;
import com.qianfeng.util.DruidUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/getAll")
public class GetAll extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id1 = req.getParameter("id");
        System.out.println(id1);

        boolean flag= id1!=null?true:false;//true 查所有 false查四个
        DruidPooledConnection connection = DruidUtil.getConnection();

        ArrayList<Bm> bms = new ArrayList<>();


        ResultSet rs=null;
        PreparedStatement ps =null;
        try {

            if(flag){
               ps = connection.prepareStatement("select * from bm");
            }else{
               ps = connection.prepareStatement("select * from bm limit 0,4");
            }

           rs = ps.executeQuery();

            while(rs.next()){

              /*  id;
                private String name;
                private Integer age;
                private String [] works;
                private String home;
                private Integer work_time;
                private String edu;
                private String [] imgs;
                private Double salary;
                private String gender;*/


                int id = rs.getInt(1);
                String name = rs.getString(2);
                int age = rs.getInt(3);
                String ws = rs.getString(4);
                String[] works = ws.split(",");
                String home = rs.getString(5);
                int work_time = rs.getInt(6);
                String edu = rs.getString(7);
                String is = rs.getString(8);
                String[] imgs = is.split(",");
                double salary = rs.getDouble(9);
                final String gender = rs.getString(10);
                Bm bm = new Bm(id,name,age,works,home,work_time,edu,imgs,salary,gender);
                bms.add(bm);

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                rs.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            try {
                ps.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            try {
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        }
        Gson gson = new Gson();
        String s = gson.toJson(bms);
        resp.setContentType("text/html;charset=utf-8");
        resp.getWriter().println(s);

    }
}
