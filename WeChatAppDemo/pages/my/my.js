// pages/my/my.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this =this;
    wx.getUserInfo({
      success: function(res) {
        var userInfo = res.userInfo
        _this.setData({
          user:userInfo
        })

      }
    })



  },
  bindKeyInput: function (e) {
    this.setData({
      inputValue: e.detail.value
    })
  },
  toOrder:function(){
     var x=this.data.inputValue;

     var reg=/^\d{11}$/
     if(!reg.test(x)){
        
      wx.showModal({
        title: '警告',
        content: '请输入正确的手机号',
        showCancel:false
      })
      return ;


     }


     wx.navigateTo({
       url: '../os/os?tel='+x,
     })
   

   
  
  }
  ,

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  toEr:function(){

      wx.navigateTo({
        url: '../er/er',
      })
    
  }
  ,

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})