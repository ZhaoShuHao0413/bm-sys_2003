package com.qianfeng.entity;

public class Bm {
    private Integer id;
    private String name;
    private Integer age;
    private String [] works;
    private String home;
    private Integer work_time;
    private String edu;
    private String [] imgs;
    private Double salary;
    private String gender;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Bm(){}

    public Bm(Integer id, String name, Integer age, String[] works, String home, Integer work_time, String edu, String[] imgs, Double salary, String gender) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.works = works;
        this.home = home;
        this.work_time = work_time;
        this.edu = edu;
        this.imgs = imgs;
        this.salary = salary;
        this.gender = gender;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String[] getWorks() {
        return works;
    }

    public void setWorks(String[] works) {
        this.works = works;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public Integer getWork_time() {
        return work_time;
    }

    public void setWork_time(Integer work_time) {
        this.work_time = work_time;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public String[] getImgs() {
        return imgs;
    }

    public void setImgs(String[] imgs) {
        this.imgs = imgs;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
