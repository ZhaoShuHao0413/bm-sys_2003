package com.qianfeng.web;

import com.alibaba.druid.pool.DruidPooledConnection;
import com.google.gson.Gson;
import com.qianfeng.entity.Order;
import com.qianfeng.util.DruidUtil;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/getOrder")
public class GetOrder extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String tel = req.getParameter("tel");


        DataSource dataSource = DruidUtil.getDataSource();

        QueryRunner queryRunner = new QueryRunner(dataSource);
        List<Order> query =null;
        try {
          query = queryRunner.query("select * from t_order where tel = ?", new BeanListHandler<Order>(Order.class),tel);
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        Gson gson = new Gson();
        String s = gson.toJson(query);
        resp.setContentType("text/html;charset=utf-8");
        resp.getWriter().println(s);

    }
}
