# BmSys_2003


一、微信小程序项目介绍
1.项目名称
	微信小程序——《动漫保姆》
	当今世界，大家都很忙，没有时间打理自己的生活，此时就需要有一个统一的，提供家政服务的平台。因此，我们的《动漫保姆》产品就问世了。通过练习，熟悉微信小程序的编写流程。

2.功能
	- 保姆列表
	- 保姆详情
	- 预约
	- 我的（订单中心）
3.架构图
	
![输入图片说明](WeChatAppDemo/imgs/imgs/image.png)

二、项目实施规划
	1.根据项目功能提取数据实体及数据项
	2.找出实体关系，绘制E-R图
	3.创建数据表
	4.分析小程序与服务器的数据接口
	5.开发小程序
	6.开发服务端（web项目）
		a.建库建表
		b.JDBC
		c.Servlet（接受小程序请求并响应）
	7.测试
	8.部署并上线

三、建立数据模型
	1.实体
		1）保姆
		2）用户
		3）订单
	2.提取数据项
		1）保姆：
				编号、姓名、年龄、性别、工作年限、薪水、教育背景、家乡、工作内容
		2）用户：
			编号、姓名、性别、地址、年龄...
		3）订单:
			编号、用户编号、保姆编号、订单时间、订单状态、备注

	3.E-R图   entity  relationship
![输入图片说明](WeChatAppDemo/imgs/imgs/image1.png)

四、小程序介绍
	1.什么是小程序
	微信小程序是微信app提出的一个小功能，可以快速展示信息，以及实现一些小功能的程序。
![输入图片说明](WeChatAppDemo/imgs/imgs/image2.png)
	
	2.优点
	1）快、小
	2）方便、功能强大
	3.应用场景
	1）商业宣传
	2）信息展示...

五、如何开发一个自己的小程序
1.在微信平台注册一个小程序
	地址：https://mp.weixin.qq.com
注册程序（QQ邮箱，实名认证的微信）
登录到微信小程序的管理界面（QQ邮箱，微信）
设置小程序基本信息（名称，简称，图标，简介，分类）
获取小程序的AppID
2.下载微信web开发者工具
3.进行小程序的开发
一定要对官方的文档很熟练
https://developers.weixin.qq.com/miniprogram/dev/framework/
1) 小程序的基本结构
2）app.json
![输入图片说明](WeChatAppDemo/imgs/imgs/image3.png)

4.小程序本地测试
5.小程序发布

六、最优保姆的开发
后端：
![输入图片说明](WeChatAppDemo/imgs/imgs/image0.png)

小程序：
![输入图片说明](WeChatAppDemo/imgs/imgs/image4.png)


![输入图片说明](WeChatAppDemo/imgs/imgs/image5.png)
![输入图片说明](WeChatAppDemo/imgs/imgs/image6.png)

![输入图片说明](WeChatAppDemo/imgs/imgs/image7.png)
![输入图片说明](WeChatAppDemo/imgs/imgs/image8.png)
![输入图片说明](WeChatAppDemo/imgs/imgs/image11.png)
![输入图片说明](WeChatAppDemo/imgs/imgs/image9.png)