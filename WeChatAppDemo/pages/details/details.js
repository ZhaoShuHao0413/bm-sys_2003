// pages/details/details.js


var audio=wx.createInnerAudioContext();//声明一个音频对象
var flag=true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    music:"stop.png",
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 2000,
    duration: 500
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this=this;
    wx.request({
      url: 'http://localhost:8080/BmSys_2003/getOne',
      data:{id:options.id},
      success(res){
        console.log(res)
        console.log(res.data)
        _this.setData({

          bm:res.data
        })
      }
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
     audio.autoplay=true;
     audio.src="http://localhost:8080/BmSys_2003/music/zzz.m4a";

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  ss:function(){

      if(flag){
          audio.pause();
          flag=false;
          this.setData({
            music:"play.png"
          })
      }else{
        audio.play();
        flag=true;
        this.setData({
          music:"stop.png"
        })
      }
  },
  toOrder:function(res){
    audio.stop();

   var id= res.target.dataset.id;
   var sal= res.target.dataset.salary;

   console.log(id+sal);
   wx.reLaunch({
    url: '../order/order?id='+id+"&salary="+sal
  })

  }
})