package com.qianfeng.web;

import com.alibaba.druid.pool.DruidPooledConnection;
import com.qianfeng.util.DruidUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet("/addOrder")
public class OrderServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String bmid = req.getParameter("bmid");
        String salary = req.getParameter("salary");
        String tel = req.getParameter("tel");
        String sdates = req.getParameter("sdates");
        String edates = req.getParameter("edates");

        DruidPooledConnection connection = DruidUtil.getConnection();
                   PreparedStatement ps =null;
        try {
            ps = connection.prepareStatement("insert into t_order (tel,bmid,salary,sdates,edates,total)values (?,?,?,?,?,?)");
            ps.setString(1,tel);
            ps.setString(2,bmid);
            ps.setString(3,salary);
            ps.setString(4,sdates);
            ps.setString(5,edates);
            ps.setString(6,"0");

            int i = ps.executeUpdate();
            resp.getWriter().println(i);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                ps.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            try {
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
