package com.qianfeng.web;


import com.alibaba.druid.pool.DruidPooledConnection;
import com.google.gson.Gson;
import com.qianfeng.util.DruidUtil;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/imgs")
public class ImgServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {



        DataSource dataSource = DruidUtil.getDataSource();
        QueryRunner runner = new QueryRunner(dataSource);

        try {
            String res = runner.query("select url from imgs where id =1", new ScalarHandler<String>());


            String[] split = res.split(",");

            Gson gson = new Gson();
            String s = gson.toJson(split);
            System.out.println(s);//输出
            resp.getWriter().println(s);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
