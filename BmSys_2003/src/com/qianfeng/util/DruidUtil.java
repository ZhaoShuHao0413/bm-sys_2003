package com.qianfeng.util;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.alibaba.druid.pool.DruidPooledConnection;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

public class DruidUtil {

    private static DruidDataSource dataSource;

    static {

        Properties properties = new Properties();
        try {
            properties.load(DruidUtil.class.getResourceAsStream("/db.properties"));
            dataSource = (DruidDataSource)DruidDataSourceFactory.createDataSource(properties);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static DruidPooledConnection getConnection(){

        try {
            return dataSource.getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        return null;
    }


    public static DataSource getDataSource(){
        return dataSource;
    }





}
