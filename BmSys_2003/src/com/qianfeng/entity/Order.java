package com.qianfeng.entity;

public class Order {

    private Integer id;
    private String tel;
    private Integer bmid;
    private Double salary;
    private String sdates;
    private String edates;
    private Double total;

    public Order(){

    }

    public Order(Integer id, String tel, Integer bmid, Double salary, String sdates, String edates, Double total) {
        this.id = id;
        this.tel = tel;
        this.bmid = bmid;
        this.salary = salary;
        this.sdates = sdates;
        this.edates = edates;
        this.total = total;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Integer getBmid() {
        return bmid;
    }

    public void setBmid(Integer bmid) {
        this.bmid = bmid;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public String getSdates() {
        return sdates;
    }

    public void setSdates(String sdates) {
        this.sdates = sdates;
    }

    public String getEdates() {
        return edates;
    }

    public void setEdates(String edates) {
        this.edates = edates;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }


}
