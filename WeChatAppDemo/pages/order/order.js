// pages/order/order.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      
      var now=new Date();
      var year=now.getFullYear();
      var month=now.getMonth()+1;
      var date=now.getDate();
      var today=year+"-"+month+"-"+date;

     
      //计算afterMonth 日期的代码
       var v=new Date();
        if (v) {
            var t = new Date(v.getFullYear(), v.getMonth(), v.getDate(), v.getHours(), v.getMinutes(), v.getSeconds(), v.getMilliseconds());
            t.setMonth(v.getMonth() + 1);
            if (t.getDate() != v.getDate()) { t.setDate(0); }
            
        }
        
        var year=t.getFullYear();
        var month=t.getMonth()+1;
        var date=t.getDate();
        var afterMonth=year+"-"+month+"-"+date;
      
      this.setData({
        today:today,
        afterMonth:afterMonth
      })
      
      var _this=this;
      wx.getUserInfo({
        success: function(res) {
          var userInfo = res.userInfo
          var nickName = userInfo.nickName
          var avatarUrl = userInfo.avatarUrl
          var gender = userInfo.gender //性别 0：未知、1：男、2：女
          gender= gender==0?'未知':gender==1?'男':'女';
          var province = userInfo.province
          var city = userInfo.city
          var country = userInfo.country
          console.log(userInfo)

          _this.setData({
            user:userInfo,
            gender:gender,
            id:options.id,
            salary:options.salary

          })

        }
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },bindDateStart: function (e) {
    console.log(e.detail.value)
    this.setData({
      sdates: e.detail.value,
      edates: ""
    })

  }, bindDateEnd: function (e) {
    console.log(e.detail.value)
    this.setData({
      edates: e.detail.value
    })},
    
  createOrder:function(res){
      console.log(res);
      var data= res.detail.value;//所有表单中提交的数据

      var sdates=data.sdates;
      var edates=data.edates;
      var tel=data.tel;

      if(sdates==""||edates==""){//日期为空不能提交
        wx.showModal({
          title: '警告',
          content: '请选择服务时间',
          showCancel:false
        })
        return ;
      }else{
        var reg=/^\d{11}$/;

        if(!reg.test(tel)){
          wx.showModal({
            title: '警告',
            content: '请输入正确手机号',
            showCancel:false
          })
          return ;
        }

          wx.request({
            url: 'http://localhost:8080/BmSys_2003/addOrder',
            data:data,
            success(res){
                if(res.data>0){//表示操作成功

                  wx.showToast({
                    title: '成功',
                    icon: 'success',
                    duration: 2000
                  })

                  setTimeout(function(){

                    wx.reLaunch({
                      url: '../index/index',
                    })
                    
                  },2000)
                 
                
                }else{//操纵失败
                  wx.showToast({
                    title: '下单失败',
                    icon: 'error',
                    duration: 2000
                  })
                }
            }
          })



      }
     
  }
})